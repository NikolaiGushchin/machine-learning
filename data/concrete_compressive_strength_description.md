# Прочность на сжатие бетона #

## Ссылка ##

https://archive.ics.uci.edu/ml/datasets/Concrete+Compressive+Strength

## Информация о наборе данных ##

Количество экземпляров 1030

Количество атрибутов 9

Разбивка атрибутов 8 количественных входных переменных и 1 количественная выходная переменная

Отсутствующие значения атрибутов Нет

## Суть задачи ##

Определить прочность сжатия бетона при определённых компонентах в течении длительного времени. 

## Информация об атрибутах ##

**Цемент (компонент 1)** - количественный - кг в смеси м3 

**Доменный шлак (компонент 2)** - количественный - кг в смеси м3 

**Летучая зола (компонент 3)** - количественная - кг в смеси м3 

**Вода (компонент 4)** - количественная - кг в смеси м3 

**Суперпластификатор (компонент 5)** - количественная - кг в смеси м3 

**Грубый заполнитель (компонент 6)** - количественный - кг в смеси м3

**Мелкого заполнителя (компонент 7)** - количественный - кг в смеси м3 

**Возраста** - количественный - день (1 ~ 365)

**Прочность бетона на сжатие** - количественная - МПа 